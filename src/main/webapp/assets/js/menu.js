$(document).ready(function () {
    $.fn.dataTable.moment('DD/MM/YYYY HH:mm');

    $('#tabela_minutas').dataTable({
        "order": [1, 'desc'],
        "language": {
            "url": "/minutaonline-artifactid/assets/Portuguese-Brasil.json"
        }
    });
});