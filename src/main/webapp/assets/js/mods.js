$(document).ready(function () {
    //Botão limpar
    $('#limpar').click(function () {
        $('#maspServidor').val("");
        $('#admissao').val("1").change();
        $('#idCargo').val("1");
        $('#idNivel').val("1");
        $('#idGrau').val("1");
        $('#nomeServidor').val("");
        $('#localidade').val("1");
        $('#idLotacao').val('1');
        lotacao($('#localidade').val(), json);

    });

    //Buscar informações de Masp Inserido

    $('.servidor').on('input', function (event) {
        event.stopPropagation();
        if ($('#maspServidor').val().length > 6) {
            buscaInformacoesServidor($('#maspServidor').val(), $('#admissao').val());
        }

    });

    //Busca informações de Localidade

    $('#localidade').change(function () {
        lotacao($('#localidade').val(), json);
    })



    //Verifica validação de Masp
    $('#maspServidor').focusout(function () {
        validaCampo('maspServidor', 'MASP inserido inválido.', validate($(this).val()));
    });

    //Verifica campos Randomicos
    $('.campo-minuta').focusout(function () {
        validaCampo($(this).attr('id'), 'Campo vazio!', isEmpty($(this)));
    });


    //Carrega informações de Localidade ao carregar a página.
    lotacao($('#localidade').val(), json);

});


//############################################# LISTA DE FUNÇÕES ###############################################3

function lotacao(idCidade, json) {
    var objJSON = jQuery.parseJSON(json);
    $('#idLotacao').empty();

    for (var i = 0; i < objJSON[idCidade].length; i++) {
        $('#idLotacao').append('<option value="' + objJSON[idCidade][i].idLotacao + '">'
            + objJSON[idCidade][i].lotacao + '</option>');
        //console.log(objJSON[idCidade][i].idLotacao + ' ' + objJSON[idCidade][i].lotacao);
    }
}

function buscaInformacoesServidor(maspServidor, admissao) {

    $.post("servidoresJSON/" + maspServidor + "/" + admissao,
        function (data, status) {
            var obj = jQuery.parseJSON(data);
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].maspServidor == maspServidor && obj[i].admissao) {
                    $('#idCargo').val(obj[i].idCargo);
                    $('#idNivel').val(obj[i].idNivel);
                    $('#idGrau').val(obj[i].idGrau);
                    $('#nomeServidor').val(obj[i].nomeServidor);
                    $('#localidade').val(obj[i].idCidade);
                    lotacao($('#localidade').val(), json);
                    $('#idLotacao').val(obj[i].idLotacao);
                }
            }
        });


}

function validaCampo(idInput, texto, bool) {

    //idCampo é a div que contém o input

    var elemento = document.getElementById(idInput).parentNode;

    $('#' + idInput).parent().removeClass("has-error");
    $('#' + idInput + "span").remove();

    if (!bool) {
        var no = document.createElement("span");
        var noTexto = document.createTextNode(texto);
        no.appendChild(noTexto);
        no.setAttribute("class", "help-block");
        no.setAttribute("id", idInput + "span");

        elemento.appendChild(no);
        elemento.setAttribute("class", "form-group has-error");
    }

}

function campoInvalido(idCampo, idInput, texto) {

    //idCampo é a diva que contém o input

    var no = document.createElement("span");
    var noTexto = document.createTextNode(texto);
    no.appendChild(noTexto);
    no.setAttribute("class", "help-block");
    no.setAttribute("id", idInput);
    window.addEventListener("load", function () {
        var elemento = document.getElementById(idCampo);
        elemento.appendChild(no);
        elemento.setAttribute("class", "form-group has-error");
    });
}


//http://jsfiddle.net/criptkiller/GVkx4/

String.prototype.reverse = function () {
    var str = this,
    newString = new String();
    for (n = str.length; n >= 0; n--) {
        newString += str.charAt(n);
    }
    return newString;
}


//Verifica se input está preenchido
function isEmpty(input) {

    if (input.val() == '') {
        return false;
    }
    return true;
}



//Valida o Masp
function validate(str) {

    var field_value = str.split("");

    if (field_value.length > 6) {

        var algarismos = [];
        var soma = 0;
        field_value.reverse();
        var dv = field_value[0];

        field_value.reverse();
        field_value.pop();
        field_value.reverse();

        for (i = 0; i < field_value.length; i++) {
            algarismos[i] = field_value[i];

            if ((i % 2) == 0) {
                algarismos[i] *= 2;
                if (algarismos[i] > 9) {
                    algarismos[i] -= 9;
                }
            }
            soma += parseInt(algarismos[i]);
        }

        soma = soma % 10;
        soma = 10 - soma;

        if(soma == 10){
            soma = 0;
        }

        if (soma == dv) {
            return true;
        }
    }
    return false;
}


