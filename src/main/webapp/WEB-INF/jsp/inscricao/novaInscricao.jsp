<%@ taglib tagdir="/WEB-INF/tags" prefix="tag"%>
<%@ taglib tagdir="/WEB-INF/tags/components" prefix="components"%>
<%@ taglib tagdir="/WEB-INF/tags/components/inscricao" prefix="inscricao"%>

<tag:header title="Nova Inscrição">

	<tag:css-basico />

</tag:header>

<components:painel panelHeader="Solicitação de Inscrição">

	<inscricao:solicitacao/>

</components:painel>

<tag:js-basico />


<tag:footer />