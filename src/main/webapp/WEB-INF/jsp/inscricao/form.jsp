<%@ taglib tagdir="/WEB-INF/tags" prefix="tag"%>
<%@ taglib tagdir="/WEB-INF/tags/components" prefix="components"%>
<%@ taglib tagdir="/WEB-INF/tags/components/inscricao" prefix="inscricao"%>

<tag:header title="Nova Inscri��o">

	<tag:css-basico />

</tag:header>

<components:painel panelHeader="Nova Inscri��o">

	<inscricao:inscricao-form servidor="${servidor}" />

</components:painel>

<tag:js-basico />

<inscricao:inscricao-form-js servidor="${servidor}"  />

<tag:footer />