<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="valueTelefone" required="true"%>
<%@attribute name="nameTelefone" required="true"%>

<div class="form-group">
	<label for="idTelefone" class="col-sm-2 control-label">Telefone(s)</label> <input
		type="text" class="form-control" name="${nameTelefone }"
		required id="idTelefone" value="${valueTelefone}" />

</div>
