<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="servidorAPI" required="true"%>
<%@attribute name="lotacaoAPI" required="true"%>


<script>
	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();
		_atualizaLotacao()

	});

	function _atualizaLotacao() {
		var inputMasp = document.getElementById('idMasp');
		var masp = inputMasp.value;
		var admissao = document.getElementById('idAdmissao').value;

		if (admissao && masp) {

			var url = "${servidorAPI}" + admissao + "/" + masp;
			// 					Faz request para verificar se o masp já está cadastrado e preenche
			var jqxhr = $
					.get(url, function(servidor) {
					})
					.done(
							function(servidor) {
								var apiLotacao = "${lotacaoAPI}lotacao/"
										+ servidor.lotacao.idLotacao;
								$
										.getJSON(apiLotacao, function(lotacao) {
										})
										.done(
												function(lotacao) {
													document
															.getElementById('idCidade').value = lotacao.cidade.idCidade;
													document
															.getElementById('idNome').value = servidor.nome;
													document
															.getElementById('idCargo').value = servidor.cargo;
													document
															.getElementById('idFuncao').value = servidor.funcao;

													selectLotacao(
															document
																	.getElementById('idCidade').value,
															lotacao.idLotacao);
												});

							}); //var jqxhr = $
		}//if (admissao && masp)
	}

	function _validate(str) {

		var field_value = str.split('');
		if (field_value.length > 6) {

			var algarismos = [];
			var soma = 0;
			field_value.reverse();
			var dv = field_value[0];
			field_value.reverse();
			field_value.pop();
			field_value.reverse();
			for (i = 0; i < field_value.length; i++) {
				algarismos[i] = field_value[i];
				if ((i % 2) == 0) {
					algarismos[i] *= 2;
					if (algarismos[i] > 9) {
						algarismos[i] -= 9;
					}
				}
				soma += parseInt(algarismos[i]);
			}

			soma = soma % 10;
			soma = 10 - soma;
			if (soma == 10) {
				soma = 0;
			}

			if (soma == dv) {
				return true;
			}
		}
		return false;
	}

	function selectLotacao(_idCidade, _idLotacao) {
		var apiLotacao = "${lotacaoAPI}" + _idCidade + "/lotacoes";

		limpaSelect('#idLotacao');

		if (_idCidade) {

			$
					.getJSON(apiLotacao, function(lotacoes) {
					})
					.done(
							function(lotacoes) {
								var html = '';
								var len = lotacoes.length;
								for (var i = 0; i < len; i++) {

									var selected = lotacoes[i].idLotacao == _idLotacao ? "selected"
											: "";

									html += '<option value="' + lotacoes[i].idLotacao + '" ' + selected +'>'
											+ lotacoes[i].lotacao + '</option>';
									if (selected) {
										console.log(html);
									}

								}
								$('#idLotacao').append(html);
							});
		}
	};

	function validaMasp() {

		var inputMasp = document.getElementById('idMasp');
		var masp = inputMasp.value;
		var admissao = document.getElementById('idAdmissao').value;

		var maspErro = document.getElementById('masp-erro');

		var bool = _validate(masp);
		if (!bool) {
			inputMasp.setAttribute("class", "form-control is-invalid");
			maspErro.innerHTML = "Masp digitado é invalido.";
		} else {
			inputMasp.setAttribute("class", "form-control");
			maspErro.innerHTML = " ";
			_atualizaLotacao();
		}

	}
</script>