<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="nameLotacao" required="true"%>


<div class="form-group">
	<label for="idCidade" class="col-sm-2 control-label">Cidade</label>
	<select id="idCidade" class="form-control"></select>
</div>
<div class="form-group">
	<label for="idLotacao" class="col-sm-2 control-label"
		>Escola</label> <select id="idLotacao" name="${nameLotacao }"
		class="form-control"></select>
</div>