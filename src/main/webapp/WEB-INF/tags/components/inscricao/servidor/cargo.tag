<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="valueCargo" required="true"%>
<%@attribute name="nameCargo" required="true"%>

<div class="form-group">
	<label for="idCargo" class="col-sm-2 control-label">Cargo</label> <select
		class="form-control" name="${nameCargo }" id="idCargo">
		<c:forEach items="${cargos}" var="cargo" varStatus="loop">
			<option value="${cargo}" ${cargo == valueCargo ? 'selected' : '' }>${cargo}</option>
		</c:forEach>
	</select>
</div>
