<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="idRegional" required="true"%>

<script>





// Remove os options do select passado	
	function limpaSelect(selectId) {
		$(selectId).find('option').remove().end();
	}
 

//Popula o select com escolas da cidade passada
	function populaSelectLotacao(_idCidade) {
		var apiLotacao = "${linkTo[LotacaoAPIController].url}" + _idCidade
				+ "/lotacoes";

		limpaSelect('#idLotacao');

		if (_idCidade) {

			$
					.getJSON(
							apiLotacao,
							function(lotacoes) {
								var html = '';
								var len = lotacoes.length;
								for (var i = 0; i < len; i++) {

									html += '<option value="' + lotacoes[i].idLotacao + '" >'
											+ lotacoes[i].lotacao + '</option>';
								}
								$('#idLotacao').append(html);
							});
		}
	};

	
//Popula o select de cidades a partir do id passado na declaração
	$('#idCidade')
			.ready(
					function() {
						var jqxhr = $
								.getJSON(
										'${linkTo[LotacaoAPIController].url}${idRegional}/cidades',
										function(cidades) {
											var html = '';
											var len = cidades.length;
											for (var i = 0; i < len; i++) {
												html += '<option value="' + cidades[i].idCidade + '" >'
														+ cidades[i].cidade
														+ '</option>';
											}
											$('#idCidade').append(html);

											populaSelectLotacao($('#idCidade')
													.val());
										});

					});
					
//Ao mudar o valor do select de cidade re popula o select de escolas
	$('#idCidade').change(function() {
		populaSelectLotacao($('#idCidade').val());
	});
</script>
