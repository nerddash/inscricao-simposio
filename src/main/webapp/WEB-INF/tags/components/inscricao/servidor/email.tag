<%@ tag language="java" pageEncoding="UTF-8"%>

<%@attribute name="valueEmail" required="true"%>
<%@attribute name="nameEmail" required="true"%>



<div class="form-group">
	<label for="idEmail" class="col-sm-2 control-label">Email</label>
	<div class="input-group mb-2 mr-sm-2 mb-sm-0">
		<span class="input-group-addon">@</span>
		<input class="form-control" type="email"
			name="${nameEmail }" required id="idEmail"
			value="${valueEmail}" required>
	</div>
</div>