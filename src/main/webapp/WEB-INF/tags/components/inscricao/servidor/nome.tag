
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="valueNome" required="true"%>
<%@attribute name="nameNome" required="true"%>

<div class="form-group">
	<label for="idNome" class="col-sm-2 control-label">Nome</label> <input
		id="idNome" name="${nameNome }" type="text"
		value="${valueNome}" class="form-control" required />
</div>