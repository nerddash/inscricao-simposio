<%@ tag language="java" pageEncoding="UTF-8"%>
<%@tag
	description="Cria campos de Masp e Admissão recebendo objeto passado."%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="valueMasp" required="true"%>
<%@attribute name="valueAdmissao" required="true"%>

<%@attribute name="nameMasp" required="true"%>
<%@attribute name="nameAdmissao" required="true"%>

<div class="form-group">
	<label for="idMasp" class="col-sm-2 control-label ">Masp</label> <input
		id="idMasp" name="${nameMasp }" type="number" class="form-control"
		min=0 value="${valueMasp}" onBlur="validaMasp()" required  data-toggle="tooltip" title="Masp com dígito sem traço ou pontos."/>
	<div id="masp-erro" class="invalid-feedback"> </div>
</div>
<div class="form-group">
	<label for="idAdmissao" class="col-sm-2 control-label">Admissão</label>
	<select id="idAdmissao" name="${nameAdmissao}" class="form-control" onBlur="validaMasp()">

		<c:forEach begin="1" end="6" varStatus="i">
			<option value="${i.index}" ${i.index == valueAdmissao ? 'selected' : '' }>0${i.index}</option>
		</c:forEach>

	</select>
</div>