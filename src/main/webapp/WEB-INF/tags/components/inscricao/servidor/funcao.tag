<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="valueFuncao" required="true"%>
<%@attribute name="nameFuncao" required="false"%>


<div class="form-group">
	<label for="idFuncao" class="col-sm-2 control-label">Função</label>
	<select class="form-control" name="${nameFuncao }" id="idFuncao">
		<c:forEach items="${funcoes}" var="funcao" varStatus="loop">
			<option value="${funcao}" ${funcao == valueFuncao ? 'selected' : '' }>${funcao.key}</option>
		</c:forEach>
	</select>
</div>

