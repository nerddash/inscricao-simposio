<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/components/inscricao/servidor"
	prefix="servidor"%>
<%@ taglib tagdir="/WEB-INF/tags/components/inscricao"
	prefix="inscricao"%>
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="servidor" type="tech.nerddash.model.Servidor"%>


<form action="${linkTo[InscricaoController].novaInscricao()}"
	method="post">

	
		<servidor:erros />
	

	<servidor:masp-adm valueMasp="${servidor.masp }"
		valueAdmissao="${servidor.admissao }" nameMasp="servidor.masp"
		nameAdmissao="servidor.admissao" />
	<servidor:nome valueNome="${servidor.nome }" nameNome="servidor.nome" />
	<servidor:cargo valueCargo="${servidor.cargo }"
		nameCargo="servidor.cargo" />
	<servidor:funcao valueFuncao="${servidor.funcao }"
		nameFuncao="servidor.funcao" />
	<servidor:lotacao nameLotacao="servidor.lotacao.idLotacao" />
	<servidor:email valueEmail="${servidor.email }"
		nameEmail="servidor.email" />
	<servidor:telefone valueTelefone="${servidor.telefone }"
		nameTelefone="servidor.telefone" />

	<inscricao:libras />

	<div class="form-group">
		<button class="btn btn-success" type="submit">Solicitar
			Inscrição</button>

	</div>


</form>