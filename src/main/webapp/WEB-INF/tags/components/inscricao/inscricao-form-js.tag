<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/components/inscricao/servidor"
	prefix="servidor"%>
<%@ taglib tagdir="/WEB-INF/tags/components/inscricao"
	prefix="inscricao"%>
	
<%@attribute name="servidor" type="tech.nerddash.model.Servidor"%>


<servidor:lotacao-js idRegional="1" />
<servidor:masp-adm-js
	servidorAPI="${environment.get('http')}${linkTo[ServidorController].servidor() }"
	lotacaoAPI="${environment.get('http')}${linkTo[LotacaoAPIController].url() }"  />
<inscricao:libras-js
	valorNecessidade="${inscricao.necessidadeEspecial }" />