<%@ tag language="java" pageEncoding="UTF-8"%>

<div class="form-group">
	<div class="form-check control-label">
		<label class="form-check-label"> <input
			class="form-check-input" type="checkbox"
			onclick="toggleDisableInputNecessidade()" id="idCheckboxNecessidade">
			Tem alguma necessidade especial?
		</label>
	</div>
	<input class="form-control" type="text"
		name="necessidadeEspecial" required id="idNecessidade"
		value="${inscricao.necessidadeEspecial}" placeholder="Qual?" disabled>
</div>
