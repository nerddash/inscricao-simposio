<%@ tag language="java" pageEncoding="UTF-8"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty sucesso }">
	<div class="alert alert-success" role="alert">${sucesso }</div>
	<div class="form-group">
		<a href="${linkTo[InscricaoController].form }"
			class="btn btn-primary active" role="button" aria-pressed="true">Nova
			Inscrição</a>

	</div>
</c:if>

<c:if test="${not empty erro }">
	<div class="alert alert-danger" role="alert">${erro }</div>
</c:if>

