<%@tag description="Forma painel principal das páginas de portal"
	pageEncoding="UTF-8"%>

<%@attribute name="panelHeader" required="true"%>

<section class="card">
	<div class="card-header text-center">
		<h4>${panelHeader}</h4>
	</div>
	<div class="card-body bg-light">
		<jsp:doBody />
	</div>
</section>