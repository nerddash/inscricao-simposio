<%@ tag language="java" pageEncoding="UTF-8"%>
<%@tag
	description="Encapusla a abertura do documento html até a abertura do Body. Seu conteúdo sera inserido no HEAD. Finalizar com a tag footer.


	Usado para importar CSS e alguma JS que deve ser iniciado antes do carregamento da página.
	
	
	Atributo 'title' requerido.
	"%>

<%@attribute name="title" required="true"%>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>${title}</title>
<jsp:doBody />
</head>
<body>