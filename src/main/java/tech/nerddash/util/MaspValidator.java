/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.util;

import java.util.ArrayList;
import java.util.Objects;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author flavio
 */
public class MaspValidator implements ConstraintValidator<ValidMasp, Long> {

    @Override
    public void initialize(ValidMasp a) {
    }

    @Override
    public boolean isValid(Long masp, ConstraintValidatorContext cvc) {

        if (masp == null) {
            return false;
        }

        Long maspInt = masp;
        ArrayList<Long> maspArrayList = new ArrayList<>();
        do {
            maspArrayList.add(maspInt % 10);
            maspInt /= 10;
        } while (maspInt > 0);

        if (maspArrayList.size() > 6) {

            Long dv = maspArrayList.remove(0);
            Long soma = Long.valueOf(0);

            for (int i = 0; i < maspArrayList.size(); i++) {
                Long numero = maspArrayList.get(i);
                if (i % 2 == 0) {
                    numero *= 2;
                    //Noves fora
                    if (numero > 9) {
                        numero -= 9;
                    }
                }
                soma += numero;
            }
            soma = soma % 10;
            soma = 10 - soma;
            if (soma == 10) {
                soma = Long.valueOf(0);
            }
            if (Objects.equals(soma, dv)) {
                return true;
            }
        }
        return false;
    }

}
