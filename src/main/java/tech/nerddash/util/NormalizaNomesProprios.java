package tech.nerddash.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NormalizaNomesProprios {

	public static String normaliza(String nomeEntrada) {

		List<String> PREPOSICOES = Arrays.asList("e", "da", "das", "de", "do", "dos");

		String nomeNormalizado = "";

		List<String> nomesArray = new ArrayList<String>();

		// Separa os nomes e passa para lista
		nomesArray = Arrays.asList(nomeEntrada.toLowerCase().split(" "));

		for (String nome : nomesArray) {

			boolean contains = PREPOSICOES.contains(nome);

			if (!contains) {
				nomeNormalizado += Character.toUpperCase(nome.charAt(0)) + nome.substring(1);
			} else {
				nomeNormalizado += nome;
			}

			nomeNormalizado += " ";
		}
		return nomeNormalizado.trim();
	}

}
