/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.util;

import java.util.ArrayList;

import tech.nerddash.exceptions.MaspInvalidoExeption;

/**
 *
 * @author flavio
 */
public class Masp {

    public Masp() {
    }

    public int verifica(int masp) throws MaspInvalidoExeption {
        if (verify(masp)) {
            return masp;
        }
        throw new MaspInvalidoExeption("O MaSP " + masp + " é inválido.");
    }

    public int geraDV(int maspSemDv) {

        int i = 0;

        int masp = Integer.parseInt("" + maspSemDv + "" + i);

        while (!verify(masp)) {
            masp = Integer.parseInt("" + maspSemDv + "" + i);
            i++;
        }
        return masp;

    }

    private boolean verify(int masp) {
        int maspInt = masp;
        ArrayList<Integer> maspArrayList = new ArrayList<>();
        do {
            maspArrayList.add(maspInt % 10);
            maspInt /= 10;
        } while (maspInt > 0);

        if (maspArrayList.size() > 6) {

            int dv = maspArrayList.remove(0);
            int soma = 0;

            for (int i = 0; i < maspArrayList.size(); i++) {
                int numero = maspArrayList.get(i);
                if (i % 2 == 0) {
                    numero *= 2;
                    //Noves fora
                    if (numero > 9) {
                        numero -= 9;
                    }
                }
                soma += numero;
            }
            soma = soma % 10;
            soma = 10 - soma;
            if (soma == 10) {
                soma = 0;
            }
            if (soma == dv) {
                return true;
            }
        }
        return false;
    }

}
