package tech.nerddash.controller;

import static br.com.caelum.vraptor.view.Results.json;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import tech.nerddash.model.Servidor;
import tech.nerddash.model.dao.ServidorDAO;

@Controller
public class ServidorController {

	private final ServidorDAO servidorDAO;
	private final Result result;

	public ServidorController() {
		this(null, null);
	}

	@Inject
	public ServidorController(ServidorDAO servidorDAO, Result result) {
		this.servidorDAO = servidorDAO;
		this.result = result;
	}

	@Get("/servidor/{admissao}/{masp}")
	public void preencheDadosDeServidor(long masp, int admissao) {
		Servidor servidor = new Servidor();

		servidor.setMasp(masp);
		servidor.setAdmissao(admissao);

		servidor = servidorDAO.buscaServidor(servidor);

		if (servidor != null) {
			result.use(json()).withoutRoot().from(servidor).include("lotacao").exclude("idServidor").exclude("email")
					.exclude("telefone").exclude("lotacao.lotacao").exclude("masp").exclude("admissao").serialize();
		}
	}

	@Get("/servidor/")
	public void servidor() {
		/*
		 * WorkAround para retornar o endereço
		 * 
		 */
	}
}
