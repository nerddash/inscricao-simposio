package tech.nerddash.controller;

import java.lang.reflect.Method;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.Future;

import javax.inject.Inject;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.http.route.Router;
import br.com.caelum.vraptor.simplemail.AsyncMailer;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import tech.nerddash.exceptions.DaoException;
import tech.nerddash.model.Cargo;
import tech.nerddash.model.Funcao;
import tech.nerddash.model.Inscricao;
import tech.nerddash.model.Servidor;
import tech.nerddash.model.dao.InscricaoDAO;
import tech.nerddash.model.dao.LotacaoDAO;
import tech.nerddash.model.dao.ServidorDAO;

@Controller
public class InscricaoController {

	private final ServidorDAO servidorDAO;
	private final InscricaoDAO inscricaoDAO;
	private final Validator validator;
	private final Result result;
	private final LotacaoDAO lotacaoDAO;
	private final AsyncMailer mailer;
	private final Environment environment;
	private final Router router;
	private int NUMERO_MAXIMO_DE_INSCICOES;
	private ResourceBundle bundle;

	@Deprecated
	public InscricaoController() {
		this(null, null, null, null, null, null, null, null, null);
	}

	@Inject
	public InscricaoController(Environment environment, Router router, LotacaoDAO lotacaoDAO, ServidorDAO servidorDAO,
			InscricaoDAO inscricaoDAO, Validator validator, Result result, AsyncMailer mailer, ResourceBundle bundle) {
		this.environment = environment;
		this.router = router;

		this.lotacaoDAO = lotacaoDAO;
		this.servidorDAO = servidorDAO;
		this.inscricaoDAO = inscricaoDAO;
		this.validator = validator;
		this.result = result;
		this.mailer = mailer;
		this.bundle = bundle;

	}

	@Get("/")
	public void form(Servidor servidor) {
		result.include("cargos", Cargo.values());
		result.include("funcoes", Funcao.values());
	}

	@Post("/")
	public Servidor novaInscricao(Servidor servidor, String necessidadeEspecial)
			throws EmailException, NoSuchMethodException, SecurityException {

		this.NUMERO_MAXIMO_DE_INSCICOES = Integer.parseInt(this.environment.get("num_de_vagas"));

		if (servidor == null) {
			result.forwardTo(this).form(null);
		}

		// Busca o servidor no banco
		Servidor servidorTemp = verificaSeJaExisteServidor(servidor);

		// Senão houver cria um novo e armazena na temp.
		if (servidorTemp == null) {
			colocaNaLotacao(servidor);
			servidorTemp = servidorDAO.novoServidor(servidor);
		} else {
			// Se já existe atualiza o servidor no banco.

			servidorTemp.setNome(servidor.getNome());
			servidorTemp.setCargo(servidor.getCargo());
			servidorTemp.setFuncao(servidor.getFuncao());
			servidorTemp.setEmail(servidor.getEmail());
			servidorTemp.setLotacao(servidor.getLotacao());
			servidorTemp.setTelefone(servidor.getTelefone());

			servidorTemp = servidorDAO.atualizaServidor(servidorTemp);
		}

		// Agora já temos um servidor do banco em servidorTemp
		// verificar se servidor já fez inscrição
		if (!verificaSeJaTemInscricao(servidorTemp)) {

			Inscricao inscricao = new Inscricao(servidorTemp, necessidadeEspecial);
			// Devolve servidor do banco para a inscrição
			inscricao.setServidor(servidorTemp);

			try {
				// Salva inscrição no banco

				Method declaredMethod = InscricaoController.class.getDeclaredMethod("validaInscricao", Integer.class);
				String form = router.urlFor(InscricaoController.class, declaredMethod, inscricao.getHash());
				Email email = new SimpleEmail();
				email.setSubject(environment.get("email_assunto"));
				email.addTo(inscricao.getServidor().getEmail());
				email.setMsg(environment.get("email_body") + environment.get("site_url") + form);

				Future<Void> asyncSend = mailer.asyncSend(email);

				if ((!asyncSend.isCancelled())
						&& (verificaNumeroDeInscricoesConfirmadas() < NUMERO_MAXIMO_DE_INSCICOES)) {

					inscricao = inscricaoDAO.registra(inscricao);

					result.include("sucesso", bundle.getString("solicitacao_confirmada"));
				} else {
					result.include("erro", bundle.getString("inscricao_encerrada"));
				}
				// tem que mudar, passar a ser dinâmica

			} catch (DaoException e) {
				validator.add(new I18nMessage("erro-inscricao", e.getMessage()));
				validator.onErrorRedirectTo(this).form(servidorTemp);
			}
		} else {
			validator.add(new I18nMessage("erro-inscricao", "ja-possui-inscricao"));
			validator.add(new I18nMessage("erro-inscricao", "email_contato"));
			validator.onErrorRedirectTo(this).form(servidorTemp);
		}

		return servidorTemp;
	}

	private Servidor verificaSeJaExisteServidor(Servidor servidor) {

		return servidorDAO.buscaServidor(servidor);
	}

	private Servidor colocaNaLotacao(Servidor servidor) {

		servidor.setLotacao(lotacaoDAO.buscaLotacao(servidor.getLotacao().getIdLotacao()));
		return servidor;

	}

	private boolean verificaSeJaTemInscricao(Servidor servidor) {
		if (inscricaoDAO.buscaInscricao(servidor) != null) {
			return true;
		}
		return false;
	}

	@Get("/{hash}")
	public void validaInscricao(Integer hash) {

		Inscricao inscricao = inscricaoDAO.buscaInscricao(hash);

		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
				.withLocale(new Locale("pt", "br"));

		if (inscricao != null) {
			// Hostname, port and security settings are made by the Mailer

			inscricao.setValidada(true);

			result.include("sucesso",
					environment.get("inscricao_confirmada") + inscricao.getDataHoraInscricao().format(formatter));
		} else {
			result.include("erro", environment.get("erro-confirmacao"));

		}

	}

	private int verificaNumeroDeInscricoesConfirmadas() {
		return inscricaoDAO.numeroDeInscricoesConfirmadas();
	}

}
