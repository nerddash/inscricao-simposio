package tech.nerddash.controller;

import static br.com.caelum.vraptor.view.Results.json;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import tech.nerddash.model.Cidade;
import tech.nerddash.model.Lotacao;
import tech.nerddash.model.Regional;
import tech.nerddash.model.dao.CidadeDAO;
import tech.nerddash.model.dao.LotacaoDAO;
import tech.nerddash.model.dao.RegionalDAO;

@Controller
public class LotacaoAPIController {

	private final RegionalDAO regionalDAO;
	private final CidadeDAO cidadeDAO;
	private final LotacaoDAO lotacaoDAO;
	private final Result result;

	@Deprecated
	public LotacaoAPIController() {
		// TODO Auto-generated constructor stub
		this(null, null, null, null);
	}

	@Inject
	public LotacaoAPIController(RegionalDAO regionalDAO, CidadeDAO cidadeDAO, LotacaoDAO lotacaoDAO, Result result) {
		this.regionalDAO = regionalDAO;
		this.cidadeDAO = cidadeDAO;
		this.lotacaoDAO = lotacaoDAO;
		this.result = result;

	}

	@Get("/api/")
	public void url() {
		/*
		 * WorkAround para retornar o endereço do controlador sem método
		 * 
		 */
	}

	// Métodos para retorno de Listagens de Objetos

	@Get("/api/regionais")
	public void JsonListaRegionais() {
		List<Regional> listaRegionais = regionalDAO.listaRegionais();
		result.use(json()).withoutRoot().from(listaRegionais).serialize();
	}

	@Get("/api/{idRegional}/cidades")
	public void JsonListaCidades(int idRegional) {
		Regional buscaRegional = regionalDAO.buscaRegional(idRegional);
		List<Cidade> listaCidades = this.cidadeDAO.listaCidades(buscaRegional);
		result.use(json()).withoutRoot().from(listaCidades).serialize();
	}

	@Get("/api/{idCidade}/lotacoes")
	public void JsonListaLotacoes(int idCidade) {
		Cidade buscaCidade = cidadeDAO.buscaCidade(idCidade);
		List<Lotacao> listaLotacaos = lotacaoDAO.listaLotacaos(buscaCidade);
		result.use(json()).withoutRoot().from(listaLotacaos).serialize();
	}

	// Metodos para retorno de Objetos simples

	@Get("/api/regional/{idRegional}")
	public void JsonRegional(int idRegional) {
		Regional buscaRegional = regionalDAO.buscaRegional(idRegional);
		result.use(json()).withoutRoot().from(buscaRegional).serialize();
	}

	@Get("/api/cidade/{idCidade}")
	public void JsonCidade(int idCidade) {
		Cidade buscaCidade = cidadeDAO.buscaCidade(idCidade);
		result.use(json()).withoutRoot().from(buscaCidade).include("regional")
				.exclude("regional.diretor", "regional.regional").serialize();
	}

	@Get("/api/lotacao/{idLotacao}")
	public void JsonLotacao(int idLotacao) {
		Lotacao buscaLotacao = lotacaoDAO.buscaLotacao(idLotacao);
		result.use(json()).withoutRoot().from(buscaLotacao).include("cidade").exclude("cidade.cidade").serialize();
	}
}
