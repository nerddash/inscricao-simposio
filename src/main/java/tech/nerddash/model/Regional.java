package tech.nerddash.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author flavio
 */

@Entity
public class Regional implements Serializable{
  
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -5893079449361663344L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idRegional;
    
    @Column(unique = true)
    @NotNull
    private String regional;
    
    @NotNull
    private String diretor;

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public String getRegional() {
        return regional;
    }
        

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public Regional() {
    }

    public Regional(String regional, String diretor) {
        this.regional = regional;
        this.diretor = diretor;
    }

    @Override
    public String toString() {
        return "Regional{" + "idregonal=" + idRegional + ", regional=" + regional + ", diretor=" + diretor + '}';
    }

    public int getIdRegional() {
        return idRegional;
    }

    public void setIdRegional(int idRegional) {
        this.idRegional = idRegional;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((diretor == null) ? 0 : diretor.hashCode());
		result = prime * result + idRegional;
		result = prime * result + ((regional == null) ? 0 : regional.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Regional other = (Regional) obj;
		if (diretor == null) {
			if (other.diretor != null)
				return false;
		} else if (!diretor.equals(other.diretor))
			return false;
		if (idRegional != other.idRegional)
			return false;
		if (regional == null) {
			if (other.regional != null)
				return false;
		} else if (!regional.equals(other.regional))
			return false;
		return true;
	}
    
    
}
