/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.model;

import java.io.Serializable;

/**
 *
 * @author flavio
 */

public enum Cargo implements Serializable {

	PEB, ASB, ATB, EEB, TDE, ANE, ANEI, AEB, ASE, DAD;

	public static Cargo getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}

	public static Cargo[] getArray() {
		return values();
	}
}
