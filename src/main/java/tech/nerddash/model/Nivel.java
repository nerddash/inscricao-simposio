package tech.nerddash.model;

import java.io.Serializable;

/**
 *
 * @author flavio
 */
public enum Nivel implements Serializable {

	UM("1 / I"), DOIS("2 / II"), TRES("3 / III"), QUATRO("4 / IV"), CINCO("5 / V"), SEIS("6 / VI");

	private String mostraNivel;

	Nivel(String nivel) {
		this.mostraNivel = nivel;
	}

	public String getKey() {
		return mostraNivel;
	}

	// Optionally and/or additionally, toString.
	@Override
	public String toString() {
		return mostraNivel;
	}

	public static Nivel getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}

	public static Nivel[] getArray() {
		return values();
	}
}
