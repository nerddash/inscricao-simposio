package tech.nerddash.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tech.nerddash.exceptions.DaoException;
import tech.nerddash.model.Regional;

/**
 *
 * @author flavio
 */
@RequestScoped
public class RegionalDAO {

	private final EntityManager em;

	@Inject
	public RegionalDAO(EntityManager em) {
		this.em = em;
	}

	public RegionalDAO() {
		this(null);
	}

	// C
	public Regional novaRegional(String nomeDaRegional, String diretor) throws DaoException {
		em.getTransaction().begin();
		Regional regional = new Regional(nomeDaRegional, diretor);
		try {
			em.persist(regional);
		} catch (Exception e) {
			throw new DaoException(
					"ERRO: \tNão foi posível criar a " + Regional.class.getSimpleName() + " " + nomeDaRegional);
		}
		em.getTransaction().commit();
		return regional;
	}

	// R
	public Regional buscaRegional() {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Regional> query = criteriaBuilder.createQuery(Regional.class);

		TypedQuery<Regional> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	public Regional buscaRegional(String nomeDaRegional) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Regional> query = criteriaBuilder.createQuery(Regional.class);

		Root<Regional> root = query.from(Regional.class);

		Path<String> regionalNome = root.<String>get("regional");

		Predicate where = criteriaBuilder.like(regionalNome, nomeDaRegional);

		query.where(where);

		TypedQuery<Regional> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	public Regional buscaRegional(Regional regional) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Regional> query = criteriaBuilder.createQuery(Regional.class);

		Root<Regional> root = query.from(Regional.class);

		Path<Integer> regionalId = root.<Integer>get("idRegional");

		Predicate where = criteriaBuilder.equal(regionalId, regional.getIdRegional());

		query.where(where);

		TypedQuery<Regional> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Regional buscaRegional(int idregional) {

		return em.find(Regional.class, idregional);
	}

	// U
	public Regional atualizaRegional(Regional regional) {
		em.getTransaction().begin();
		Regional r = em.merge(regional);
		em.getTransaction().commit();
		return r;

	}

	// D
	public void deletaRegional(Regional regional) {
		em.getTransaction().begin();
		em.remove(regional);
		em.getTransaction().commit();
	}

	// L
	public List<Regional> listaRegionais() {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Regional> query = criteriaBuilder.createQuery(Regional.class);

		Root<Regional> root = query.from(Regional.class);
		Order[] orderBy = { criteriaBuilder.asc(root.get("regional")) };

		query.select(root);

		query.orderBy(orderBy);

		TypedQuery<Regional> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();

	}

	public List<Regional> listaRegionais(String nomeDaRegional) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Regional> query = criteriaBuilder.createQuery(Regional.class);

		Root<Regional> root = query.from(Regional.class);

		List<Predicate> predicates = new ArrayList<>();

		Path<String> regionalNome = root.<String>get("regional");

		predicates.add(criteriaBuilder.like(regionalNome, "%" + nomeDaRegional + "%"));

		Order[] orderBy = { criteriaBuilder.asc(root.get("regional")) };

		query.where(predicates.toArray(new Predicate[0]));

		query.orderBy(orderBy);

		TypedQuery<Regional> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();

	}
}
