package tech.nerddash.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tech.nerddash.exceptions.DaoException;
import tech.nerddash.model.Cidade;
import tech.nerddash.model.Regional;

/**
 *
 * @author flavio
 */
@RequestScoped
public class CidadeDAO {

	private final EntityManager em;

	@Inject
	public CidadeDAO(EntityManager em) {
		this.em = em;
	}

	public CidadeDAO() {
		this(null);
	}

	// C
	public Cidade novaCidade(Regional regional, String nomeDaCidade) throws DaoException {
		em.getTransaction().begin();
		Cidade cidade = new Cidade(regional, nomeDaCidade);
		try {
			em.persist(cidade);
		} catch (Exception e) {
			throw new DaoException(
					"ERRO: \tNão foi posível criar a " + Cidade.class.getSimpleName() + " " + nomeDaCidade);
		}
		em.getTransaction().commit();
		return cidade;
	}

	// R
	public Cidade buscaCidade(String nomeDaCidade) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Cidade> query = criteriaBuilder.createQuery(Cidade.class);

		Root<Cidade> root = query.from(Cidade.class);

		Path<String> cidadeNome = root.<String>get("cidade");

		Predicate where = criteriaBuilder.equal(cidadeNome, nomeDaCidade);

		query.where(where);

		TypedQuery<Cidade> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Cidade buscaCidade(Cidade cidade) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Cidade> query = criteriaBuilder.createQuery(Cidade.class);

		Root<Cidade> root = query.from(Cidade.class);

		Path<Integer> cidadeId = root.<Integer>get("idCidade");

		Predicate where = criteriaBuilder.equal(cidadeId, cidade.getIdCidade());

		query.where(where);

		TypedQuery<Cidade> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Cidade buscaCidade(int idcidade) {

		return em.find(Cidade.class, idcidade);
	}

	// U
	public Cidade atualizaCidade(Cidade cidade) {
		em.getTransaction().begin();
		Cidade merge = em.merge(cidade);
		em.getTransaction().commit();
		return merge;
	}

	// D
	public void deletaCidade(Cidade cidade) {
		em.getTransaction().begin();
		em.remove(cidade);
		em.getTransaction().commit();
	}

	// L
	public List<Cidade> listaCidades(Regional regional) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Cidade> query = criteriaBuilder.createQuery(Cidade.class);

		Root<Cidade> root = query.from(Cidade.class);

		Path<Regional> regionalPath = root.<Regional>get("regional");

		Predicate where = criteriaBuilder.equal(regionalPath, regional);

		Order[] orderBy = { criteriaBuilder.asc(root.get("cidade")) };

		query.where(where);

		query.orderBy(orderBy);

		TypedQuery<Cidade> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();

	}

	public List<Cidade> listaCidades(Regional regional, String siglaDoCidade) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Cidade> query = criteriaBuilder.createQuery(Cidade.class);

		Root<Cidade> root = query.from(Cidade.class);

		Path<String> nomeCidade = root.<String>get("cidade");
		Path<Regional> regionalPath = root.<Regional>get("regional");

		List<Predicate> predicates = new ArrayList<>();

		predicates.add(criteriaBuilder.like(nomeCidade, "%" + siglaDoCidade + "%"));
		predicates.add(criteriaBuilder.equal(regionalPath, regional));

		Order[] orderBy = { criteriaBuilder.asc(root.get("cidade")) };

		query.select(root);

		query.where(predicates.toArray(new Predicate[0]));

		query.orderBy(orderBy);

		TypedQuery<Cidade> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();
	}

}
