/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tech.nerddash.model.Lotacao;
import tech.nerddash.model.Servidor;

/**
 *
 * @author flavio
 */
@RequestScoped
public class ServidorDAO {

	private final EntityManager em;

	@Inject
	public ServidorDAO(EntityManager em) {
		this.em = em;
	}

	public ServidorDAO() {
		this(null);
	}

	// C
	public Servidor novoServidor() {

		Servidor servidor = new Servidor();

		em.persist(servidor);

		return servidor;
	}

	public Servidor novoServidor(Servidor servidor) {
		em.persist(servidor);
		return servidor;
	}

	// R
	public Servidor buscaServidor(String nomeDoServidor) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Servidor> query = criteriaBuilder.createQuery(Servidor.class);

		Root<Servidor> root = query.from(Servidor.class);

		List<Predicate> predicates = new ArrayList<>();

		Path<String> servidorNome = root.<String>get("nome");

		predicates.add(criteriaBuilder.like(servidorNome, "%" + nomeDoServidor + "%"));

		Order[] orderBy = { criteriaBuilder.asc(root.get("nome")) };

		query.where(predicates.toArray(new Predicate[0]));

		query.orderBy(orderBy);

		TypedQuery<Servidor> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	public Servidor buscaServidor(Servidor servidor) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Servidor> query = criteriaBuilder.createQuery(Servidor.class);

		Root<Servidor> root = query.from(Servidor.class);

		List<Predicate> predicates = new ArrayList<>();

		Path<Long> servidorMasp = root.<Long>get("masp");

		Path<Integer> servidorAdmissao = root.<Integer>get("admissao");

		predicates.add(criteriaBuilder.equal(servidorMasp, servidor.getMasp()));

		predicates.add(criteriaBuilder.equal(servidorAdmissao, servidor.getAdmissao()));

		query.where(predicates.toArray(new Predicate[0]));

		TypedQuery<Servidor> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	// U
	public Servidor atualizaServidor(Servidor servidor) {

		Servidor merge = em.merge(servidor);

		return merge;

	}

	// D
	public void deletaServidor(Servidor servidor) {

		em.remove(servidor);

	}

	// L
	public List<Servidor> listaServidores() {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Servidor> query = criteriaBuilder.createQuery(Servidor.class);

		Root<Servidor> root = query.from(Servidor.class);
		Order[] orderBy = { criteriaBuilder.asc(root.get("nome")) };

		query.select(root);

		query.orderBy(orderBy);

		TypedQuery<Servidor> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();
	}

	public List<Servidor> listaServidores(Lotacao lotacao) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Servidor> query = criteriaBuilder.createQuery(Servidor.class);

		Root<Servidor> root = query.from(Servidor.class);

		List<Predicate> predicates = new ArrayList<>();
		Path<Lotacao> servidorLotacao = root.<Lotacao>get("lotacao");

		predicates.add(criteriaBuilder.equal(servidorLotacao, lotacao));

		Order[] orderBy = { criteriaBuilder.asc(root.get("nome")) };

		query.where(predicates.toArray(new Predicate[0]));

		query.orderBy(orderBy);

		TypedQuery<Servidor> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();
	}

	public List<Servidor> listaServidores(String nomeServidor) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Servidor> query = criteriaBuilder.createQuery(Servidor.class);

		Root<Servidor> root = query.from(Servidor.class);

		List<Predicate> predicates = new ArrayList<>();

		Path<String> servidorNome = root.<String>get("nome");

		predicates.add(criteriaBuilder.like(servidorNome, "%" + nomeServidor + "%"));

		Order[] orderBy = { criteriaBuilder.asc(root.get("nome")) };

		query.where(predicates.toArray(new Predicate[0]));

		query.orderBy(orderBy);

		TypedQuery<Servidor> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();
	}
}
