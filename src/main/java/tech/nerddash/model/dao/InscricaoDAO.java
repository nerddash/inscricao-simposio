package tech.nerddash.model.dao;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tech.nerddash.exceptions.DaoException;
import tech.nerddash.model.Inscricao;
import tech.nerddash.model.Servidor;

@RequestScoped
public class InscricaoDAO {

	private final EntityManager entityManager;

	@Deprecated
	public InscricaoDAO() {
		this(null);
	}

	@Inject
	public InscricaoDAO(EntityManager entityManager) {
		this.entityManager = entityManager;

	}

	public Inscricao registra(Inscricao inscricao) throws DaoException {

		try {
			entityManager.persist(inscricao);
		} catch (Exception e) {
			throw new DaoException("ERRO: \tNão foi posível registrar a Inscrição");
		}
		return inscricao;

	}

	public Inscricao buscaInscricao(Servidor servidor) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Inscricao> query = criteriaBuilder.createQuery(Inscricao.class);

		Root<Inscricao> root = query.from(Inscricao.class);

		Path<Long> servidorBanco = root.<Long>get("servidor");

		Predicate where = criteriaBuilder.equal(servidorBanco, servidor.getIdServidor());
		query.where(where);

		TypedQuery<Inscricao> typedQuery = entityManager.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	public Inscricao buscaInscricao(int hash) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Inscricao> query = criteriaBuilder.createQuery(Inscricao.class);

		Root<Inscricao> root = query.from(Inscricao.class);

		Path<Integer> hashBanco = root.<Integer>get("hash");

		Predicate where = criteriaBuilder.equal(hashBanco, hash);
		query.where(where);

		TypedQuery<Inscricao> typedQuery = entityManager.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	public int numeroDeInscricoesConfirmadas() {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Inscricao> query = criteriaBuilder.createQuery(Inscricao.class);

		Root<Inscricao> root = query.from(Inscricao.class);

		Path<Boolean> validada = root.<Boolean>get("validada");

		Predicate where = criteriaBuilder.isTrue(validada);
		query.where(where);

		TypedQuery<Inscricao> typedQuery = entityManager.createQuery(query);

		try {
			return typedQuery.getResultList().size();
		} catch (Exception e) {
			return 0;
		}
	}

}
