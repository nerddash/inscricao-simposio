package tech.nerddash.model.dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tech.nerddash.exceptions.DaoException;
import tech.nerddash.model.Cidade;
import tech.nerddash.model.Lotacao;

/**
 *
 * @author flavio
 */
@RequestScoped
public class LotacaoDAO {

	private final EntityManager em;

	@Inject
	public LotacaoDAO(EntityManager em) {
		this.em = em;
	}

	@Deprecated
	public LotacaoDAO() {
		this(null);
	}

	// C
	public Lotacao novaLotacao(Cidade cidade, String nomeDaLotacao) throws DaoException {

		Lotacao lotacao = new Lotacao(cidade, nomeDaLotacao);
		try {
			em.persist(lotacao);
		} catch (Exception e) {
			throw new DaoException(
					"ERRO: \tNão foi posível criar a " + Lotacao.class.getSimpleName() + " " + nomeDaLotacao);
		}
		return lotacao;
	}

	// R
	public Lotacao buscaLotacao(String nomeDaLotacao) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Lotacao> query = criteriaBuilder.createQuery(Lotacao.class);

		Root<Lotacao> root = query.from(Lotacao.class);

		Path<String> lotacaoNome = root.<String>get("lotacao");

		Predicate where = criteriaBuilder.equal(lotacaoNome, nomeDaLotacao);

		query.where(where);

		TypedQuery<Lotacao> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Lotacao buscaLotacao(Lotacao lotacao) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Lotacao> query = criteriaBuilder.createQuery(Lotacao.class);

		Root<Lotacao> root = query.from(Lotacao.class);

		Path<Integer> lotacaoId = root.<Integer>get("idLotacao");

		Predicate where = criteriaBuilder.equal(lotacaoId, lotacao.getIdLotacao());

		query.where(where);

		TypedQuery<Lotacao> typedQuery = em.createQuery(query);

		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Lotacao buscaLotacao(int idlotacao) {

		return em.find(Lotacao.class, idlotacao);
	}

	// U
	public Lotacao atualizaLotacao(Lotacao lotacao) {
		Lotacao merge = em.merge(lotacao);
		return merge;
	}

	// D
	public void deletaLotacao(Lotacao lotacao) {
		em.getTransaction().begin();
		em.remove(lotacao);
		em.getTransaction().commit();

	}

	// L
	public List<Lotacao> listaLotacaos(Cidade cidade) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Lotacao> query = criteriaBuilder.createQuery(Lotacao.class);

		Root<Lotacao> root = query.from(Lotacao.class);

		Path<Cidade> cidadePath = root.<Cidade>get("cidade");

		Predicate where = criteriaBuilder.equal(cidadePath, cidade);

		Order[] orderBy = { criteriaBuilder.asc(root.get("lotacao")) };

		query.where(where);

		query.orderBy(orderBy);

		TypedQuery<Lotacao> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();

	}

	public List<Lotacao> listaLotacoes(String siglaDoLotacao) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Lotacao> query = criteriaBuilder.createQuery(Lotacao.class);

		Root<Lotacao> root = query.from(Lotacao.class);

		Path<String> nomeLotacao = root.<String>get("lotacao");

		Predicate where = criteriaBuilder.like(nomeLotacao, siglaDoLotacao);

		Order[] orderBy = { criteriaBuilder.asc(root.get("lotacao")) };

		query.select(root);

		query.where(where);

		query.orderBy(orderBy);

		TypedQuery<Lotacao> typedQuery = em.createQuery(query);

		return typedQuery.getResultList();
	}

}
