package tech.nerddash.model;

import java.io.Serializable;

/**
 *
 * @author flavio
 */
public enum Grau implements Serializable {
	A, B, C, D, E, F, G, H, I, J, L, M, N, O, P;

	public static Grau getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}

	public static Grau[] getArray() {
		return values();
	}
}
