package tech.nerddash.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author flavio
 */
@Entity
public class Cidade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6923557078152918323L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCidade;

	@Column(unique = true)
	@NotNull
	private String cidade;

	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Regional regional;

	public String getCidade() {
		return cidade;
	}

	public Cidade() {
	}

	public Cidade(Regional regional, String cidade) {
		this.cidade = cidade;
		this.regional = regional;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Regional getRegional() {
		return regional;
	}

	public void setRegional(Regional regional) {
		this.regional = regional;
	}

	public int getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(int idCidade) {
		this.idCidade = idCidade;
	}

	@Override
	public String toString() {
		return "Cidade{" + "idcidade=" + idCidade + ", cidade=" + cidade + ", regional=" + regional + '}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + idCidade;
		result = prime * result + ((regional == null) ? 0 : regional.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (idCidade != other.idCidade)
			return false;
		if (regional == null) {
			if (other.regional != null)
				return false;
		} else if (!regional.equals(other.regional))
			return false;
		return true;
	}

}
