/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.model;

import java.io.Serializable;

/**
 *
 * @author flavio
 */

public enum Funcao implements Serializable {

	PROFESSOR("Professor"), SERVICAL("Serviçal"), DIRETOR("Diretor"), VICE("Vice-Diretor"), SECRETARIO(
			"Secretário"), ATB("ATB"), ESPECIALISTA("Especialista"), OUTRO("Outra função");

	private String mostraFuncao;

	Funcao(String funcao) {
		this.mostraFuncao = funcao;
	}

	public String getKey() {
		return mostraFuncao;
	}

	// Optionally and/or additionally, toString.
	@Override
	public String toString() {
		return mostraFuncao;
	}

	public static Funcao getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}

	public static Funcao[] getArray() {
		return values();
	}
}
