/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Email;

import tech.nerddash.util.NormalizaNomesProprios;
import tech.nerddash.util.ValidMasp;

/**
 *
 * @author flavio
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "masp", "admissao" }) })
public class Servidor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5238480796124311493L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idServidor;

	public Servidor() {
	}

	@NotNull
	@ValidMasp
	private Long masp;

	@NotNull
	private int admissao;

	@NotNull
	private String nome;

	@Enumerated(EnumType.STRING)
	private Cargo cargo;

	@Enumerated(EnumType.STRING)
	private Nivel nivel;

	@Enumerated(EnumType.STRING)
	private Grau grau;

	@Enumerated(EnumType.STRING)
	private Funcao funcao;

	@Email
	private String email;

	private String telefone;

	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Lotacao lotacao;

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Nivel getNivel() {
		return nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public Grau getGrau() {
		return grau;
	}

	public void setGrau(Grau grau) {
		this.grau = grau;
	}

	public Lotacao getLotacao() {
		return lotacao;
	}

	public void setLotacao(Lotacao lotacao) {
		this.lotacao = lotacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = NormalizaNomesProprios.normaliza(nome);
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.toLowerCase();
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Long getIdServidor() {
		return idServidor;
	}

	public void setIdServidor(Long idServidor) {
		this.idServidor = idServidor;
	}

	public Long getMasp() {
		return masp;
	}

	public void setMasp(Long masp) {
		this.masp = masp;
	}

	public int getAdmissao() {
		return admissao;
	}

	public void setAdmissao(int admissao) {
		this.admissao = admissao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + admissao;
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((funcao == null) ? 0 : funcao.hashCode());
		result = prime * result + ((grau == null) ? 0 : grau.hashCode());
		result = prime * result + ((idServidor == null) ? 0 : idServidor.hashCode());
		result = prime * result + ((lotacao == null) ? 0 : lotacao.hashCode());
		result = prime * result + ((masp == null) ? 0 : masp.hashCode());
		result = prime * result + ((nivel == null) ? 0 : nivel.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servidor other = (Servidor) obj;
		if (admissao != other.admissao)
			return false;
		if (cargo != other.cargo)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (funcao != other.funcao)
			return false;
		if (grau != other.grau)
			return false;
		if (idServidor == null) {
			if (other.idServidor != null)
				return false;
		} else if (!idServidor.equals(other.idServidor))
			return false;
		if (lotacao == null) {
			if (other.lotacao != null)
				return false;
		} else if (!lotacao.equals(other.lotacao))
			return false;
		if (masp == null) {
			if (other.masp != null)
				return false;
		} else if (!masp.equals(other.masp))
			return false;
		if (nivel != other.nivel)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Servidor [idServidor=" + idServidor + ", masp=" + masp + ", admissao=" + admissao + ", nome=" + nome
				+ ", cargo=" + cargo + ", nivel=" + nivel + ", grau=" + grau + ", funcao=" + funcao + ", email=" + email
				+ ", telefone=" + telefone + ", lotacao=" + lotacao + "]";
	}

}
