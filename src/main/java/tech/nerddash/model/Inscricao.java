package tech.nerddash.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "servidor_idServidor" }) })
public class Inscricao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9169899508972210450L;

	@Deprecated
	public Inscricao() {
		this(null, null);
	}

	public Inscricao(Servidor servidor, String necessidadeEspecial) {
		this.servidor = servidor;
		this.necessidadeEspecial = necessidadeEspecial;
		this.dataHoraInscricao = LocalDateTime.now();
		this.hash = this.dataHoraInscricao.hashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idInscricao;

	@NotNull
	private int hash;

	private boolean validada;

	private String necessidadeEspecial;

	@OneToOne(fetch = FetchType.EAGER)
	private Servidor servidor;

	@NotNull
	private LocalDateTime dataHoraInscricao;

	public Long getIdInscricao() {
		return idInscricao;
	}

	public void setIdInscricao(Long idInscricao) {
		this.idInscricao = idInscricao;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getHash() {
		return hash;
	}

	public LocalDateTime getDataHoraInscricao() {
		return dataHoraInscricao;
	}

	public String getNecessidadeEspecial() {
		return necessidadeEspecial;
	}

	public void setNecessidadeEspecial(String necessidadeEspecial) {
		this.necessidadeEspecial = necessidadeEspecial;
	}

	public boolean isValidada() {
		return validada;
	}

	public void setValidada(boolean validada) {
		this.validada = validada;
	}

	@Override
	public String toString() {
		return "Inscricao [idInscricao=" + idInscricao + ", hash=" + hash + ", validada=" + validada
				+ ", necessidadeEspecial=" + necessidadeEspecial + ", servidor=" + servidor + ", dataHoraInscricao="
				+ dataHoraInscricao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataHoraInscricao == null) ? 0 : dataHoraInscricao.hashCode());
		result = prime * result + ((idInscricao == null) ? 0 : idInscricao.hashCode());
		result = prime * result + ((necessidadeEspecial == null) ? 0 : necessidadeEspecial.hashCode());
		result = prime * result + ((servidor == null) ? 0 : servidor.hashCode());
		result = prime * result + (validada ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inscricao other = (Inscricao) obj;
		if (dataHoraInscricao == null) {
			if (other.dataHoraInscricao != null)
				return false;
		} else if (!dataHoraInscricao.equals(other.dataHoraInscricao))
			return false;
		if (hash != other.hash)
			return false;
		if (idInscricao == null) {
			if (other.idInscricao != null)
				return false;
		} else if (!idInscricao.equals(other.idInscricao))
			return false;
		if (necessidadeEspecial == null) {
			if (other.necessidadeEspecial != null)
				return false;
		} else if (!necessidadeEspecial.equals(other.necessidadeEspecial))
			return false;
		if (servidor == null) {
			if (other.servidor != null)
				return false;
		} else if (!servidor.equals(other.servidor))
			return false;
		if (validada != other.validada)
			return false;
		return true;
	}

}
