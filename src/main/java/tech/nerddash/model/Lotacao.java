package tech.nerddash.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author flavio
 */
@Entity
public class Lotacao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6011808097089380403L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLotacao;

	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Cidade cidade;

	@NotNull
	private String lotacao;

	public Lotacao() {
	}

	public Lotacao(Cidade cidade, String lotacao) {
		this.cidade = cidade;
		this.lotacao = lotacao;
	}

	public int getIdLotacao() {
		return idLotacao;
	}

	public void setIdLotacao(int idLotacao) {
		this.idLotacao = idLotacao;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getLotacao() {
		return lotacao;
	}

	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}

	@Override
	public String toString() {
		return "Lotacao{" + "idlotacao=" + idLotacao + ", cidade=" + cidade + ", lotacao=" + lotacao + '}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + idLotacao;
		result = prime * result + ((lotacao == null) ? 0 : lotacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lotacao other = (Lotacao) obj;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (idLotacao != other.idLotacao)
			return false;
		if (lotacao == null) {
			if (other.lotacao != null)
				return false;
		} else if (!lotacao.equals(other.lotacao))
			return false;
		return true;
	}

}
