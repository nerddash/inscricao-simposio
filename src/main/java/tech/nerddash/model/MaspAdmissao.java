package tech.nerddash.model;

import tech.nerddash.exceptions.MaspInvalidoExeption;
import tech.nerddash.util.Masp;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author flavio
 */
@Embeddable
public class MaspAdmissao implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3954658594179145936L;
    private int masp = -1;
    private int admissao = -1;


    public MaspAdmissao() {

    }

    public MaspAdmissao(int masp, int admissao) throws MaspInvalidoExeption {
        setMasp(masp);
        this.admissao = admissao;
    }

    public int getMasp() {
        return masp;
    }

    public void setMasp(int masp) throws MaspInvalidoExeption {
        this.masp = new Masp().verifica(masp);
    }

    public int getAdmissao() {
        return admissao;
    }

    public void setAdmissao(int admissao) {
        this.admissao = admissao;
    }

    @Override
    public String toString() {
        return getMasp() + " adm 0" + getAdmissao();
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + admissao;
		result = prime * result + masp;
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaspAdmissao other = (MaspAdmissao) obj;
		if (admissao != other.admissao)
			return false;
		if (masp != other.masp)
			return false;
		return true;
	}

}
