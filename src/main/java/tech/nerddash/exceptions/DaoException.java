/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.nerddash.exceptions;

/**
 *
 * @author flavio
 */
public class DaoException extends Exception {

    private static final long serialVersionUID = 1149241039409861915L;

    // constrói um objeto NumeroNegativoException com a mensagem passada por parâmetro
    public DaoException(String msg) {
        super(msg);
    }

    // contrói um objeto NumeroNegativoException com mensagem e a causa dessa exceção, utilizado para encadear exceptions
    public DaoException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
