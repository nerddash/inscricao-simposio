package tech.nerddash.util;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class NormalizaNomesPropriosTest {

	private String nomeEntrada;
	private String nomeSaidaEsperada;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

		nomeEntrada = "";
		nomeSaidaEsperada = "";

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void normalizaNomesTodosEmMinuscula() {
		nomeEntrada = "flávio barcelos";
		nomeSaidaEsperada = "Flávio Barcelos";

		assertEquals(nomeSaidaEsperada, NormalizaNomesProprios.normaliza(nomeEntrada));
	}

	@Test
	public final void normalizaNomesTodosEmMaiuscula() {
		nomeEntrada = "FLÁVIO BARCELOS";
		nomeSaidaEsperada = "Flávio Barcelos";

		assertEquals(nomeSaidaEsperada, NormalizaNomesProprios.normaliza(nomeEntrada));
	}

	@Test
	public final void normalizaNomesComCaracteresEspeciais() {
		nomeEntrada = "jari Mäenpää Sant'anna D'arck";
		nomeSaidaEsperada = "Jari Mäenpää Sant'anna D'arck";

		assertEquals(nomeSaidaEsperada, NormalizaNomesProprios.normaliza(nomeEntrada));
	}

	@Test
	public final void normalizaNomesComPreposições() {
		nomeEntrada = "Flávio arantes do Amorim e Barcelos";
		nomeSaidaEsperada = "Flávio Arantes do Amorim e Barcelos";

		assertEquals(nomeSaidaEsperada, NormalizaNomesProprios.normaliza(nomeEntrada));
	}

}
